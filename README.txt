*Please change the path of the input file in Main.java file line #171
*Please change the name of the package

In order to Contribute :
	1. Please take one of the versions of the project
	2. Copy the java files to a single folder
	3. Have a look at the input file format
		The format something like this
		ProcessesNumber , read/write operation , The location of where the read/write is done
	4. Compile and execute the java files thats it!!!!!!
	
	This is open to all contributers.

An overview of the Project

*There are 2 different versions of our simulation
	One version uses simple clock replacement algorithm for  replacement of frames
	Another version uses complex clock replacement algorithm for replacement of frames

* With  input file consisting of significantly higher number of processes and their corresponding page accesses, 
it is found that number of page faults that will occur by using complex clock replacement algorithm  is less 
than that of  by using simple clock replacement algorithm.

Team Members:

Abdus Salam Khazi	-1PI10CS001
Abhishek A. R.		-1PI10CS003
Abhishek Patil		-1PI10CS004
Akshay Mallya		-1PI10CS010
